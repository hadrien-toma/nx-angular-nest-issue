# https://github.com/nrwl/nx/issues/3982

## Current Behavior

I have errors when trying to import a Nest module (which is inside a library) into a Nest application. If the path does not use the "@" shortcut for the imports, this works (either by specifying a relative path with '../[...]' or absolutely from the workspace like 'libs/[...]').

## Expected Behavior

Do not have those errors.

## Steps to Reproduce

Here are the steps to obtain a Minimal Verifiable Complete Example. Those leaded me to [this repository](https://gitlab.com/hadrien-toma/nx-angular-nest-issue) if you just want to clone it and try to reproduce the bug inside it.

What I have done to get the above mentioned repository:

```sh
> npx create-nx-workspace@latest workspace --preset=empty --cli=nx --interactive=false --nx-cloud=false \
> cd workspace
> yarn add @nrwl/nest
> yarn run nx generate @nrwl/nest:application --name=backend
> yarn run nx generate @nrwl/nest:library --name=micro-service --buildable
```

- Then in the app, I removed all files except `app.module.ts` and put this inside it:

```
import { Module } from '@nestjs/common';
import { MicroServiceModule } from '@workspace/micro-service';

@Module({
	imports: [MicroServiceModule]
})
export class AppModule {}
```

- In `workspace/libs/micro-service/src/lib`, I removed all files and created/used the following ones instead:

#### workspace/libs/micro-service/src/lib/micro-service.controller.ts

```ts
import { MicroServiceService } from './micro-service.service';
import {
	Controller,
	Get
	} from '@nestjs/common';

@Controller('micro-service')
export class MicroServiceController {
	constructor(private readonly microServiceService: MicroServiceService) {}

	@Get()
	async get() {
		const responseBody: Promise<{id: string}[]> = this.microServiceService.get();
		return responseBody;
	}
}
```

#### workspace/libs/micro-service/src/lib/micro-service.module.ts

```ts
import { Module } from '@nestjs/common';
import { MicroServiceController } from './micro-service.controller';
import { MicroServiceService } from './micro-service.service';

@Module({
	imports: [],
	controllers: [MicroServiceController],
	providers: [MicroServiceService],
	exports: []
})
export class MicroServiceModule {}
```

#### workspace/libs/micro-service/src/lib/micro-service.service.ts

```ts
import { Injectable } from '@nestjs/common';
import {
	Observable,
	of
	} from 'rxjs';

@Injectable()
export class MicroServiceService {
	async get() {
		const responseBody: Observable<{id: string}[]> = of([
			{
				id: '1'
			}, {
				id: '2'
			}
		]);
		return responseBody.toPromise();
	}
}
```

Finally I ran `yarn run nx serve --project=backend` and got the following failure logs.

### Failure Logs

```sh
> yarn run nx serve --project=backend

ERROR in ./apps/backend/src/app/app.module.ts
Module not found: Error: Can't resolve '@workspace/micro-service' in '/home/hadrien_toma_1/gitlab/nx-angular-nest-issue/workspace/apps/backend/src/app'
There was an error with the build. See above.
/home/hadrien_toma_1/gitlab/nx-angular-nest-issue/workspace/dist/apps/backend/main.js was not restarted.
ERROR in /home/hadrien_toma_1/gitlab/nx-angular-nest-issue/workspace/apps/backend/src/app/app.module.ts(2,36):
TS2307: Cannot find module '@workspace/micro-service' or its corresponding type declarations.
Version: typescript 4.0.5
Time: 13317ms
```

### Environment

```sh
yarn run v1.21.1
$ nx report

>  NX  Report complete - copy this into the issue template

  nx : Not Found
  @nrwl/angular : Not Found
  @nrwl/cli : 10.3.1
  @nrwl/cypress : Not Found
  @nrwl/eslint-plugin-nx : 10.3.1
  @nrwl/express : Not Found
  @nrwl/jest : 10.3.1
  @nrwl/linter : 10.3.1
  @nrwl/nest : 10.3.1
  @nrwl/next : Not Found
  @nrwl/node : 10.3.1
  @nrwl/react : Not Found
  @nrwl/schematics : Not Found
  @nrwl/tao : 10.3.1
  @nrwl/web : Not Found
  @nrwl/workspace : 10.3.1
  typescript : 4.0.5

Done in 1.76s.
```