import { Module } from '@nestjs/common';
import { MicroServiceModule } from '@workspace/micro-service';

@Module({
	imports: [MicroServiceModule]
})
export class AppModule {}
