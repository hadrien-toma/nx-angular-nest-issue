import { Injectable } from '@nestjs/common';
import {
	Observable,
	of
	} from 'rxjs';

@Injectable()
export class MicroServiceService {
	async get() {
		const responseBody: Observable<{id: string}[]> = of([
			{
				id: '1'
			}, {
				id: '2'
			}
		]);
		return responseBody.toPromise();
	}
}
