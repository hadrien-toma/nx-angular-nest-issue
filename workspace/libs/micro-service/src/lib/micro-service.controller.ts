import { MicroServiceService } from './micro-service.service';
import {
	Controller,
	Get
	} from '@nestjs/common';

@Controller('micro-service')
export class MicroServiceController {
	constructor(private readonly microServiceService: MicroServiceService) {}

	@Get()
	async get() {
		const responseBody: Promise<{id: string}[]> = this.microServiceService.get();
		return responseBody;
	}
}
